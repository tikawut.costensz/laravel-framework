<?php

use App\Http\Controller\API\CompanyController;
use App\Http\Controller\API\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//http://localhost/laravel/public/api
Route::get('/',function(){
    return "Hello API";
});

Route::get('/staff/{id}',function($id){
    return "id : $id";
});

//Route::get('/company',function(){
//    return "Hello COMPANY";
//});

Route::get('/company',[CompanyController::class,'index']);
Route::get('/company/{email}',[CompanyController::class,'contact']);

Route::apiResource('/product',ProductController::class);


